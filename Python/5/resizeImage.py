def fileName():
    import Tkinter,tkFileDialog,getpass
    root=Tkinter.Tk()
    root.withdraw()
    #imgFile=tkFileDialog.askopenfile(parent=root, mode='r', filetypes = (("jpeg files","*.jpg")) )
    imgFile = tkFileDialog.askopenfilename(initialdir = '/Users/'+getpass.getuser()+'/Pictures',title = "Select file",filetypes = (("jpeg files","*.jpg"),("all files","*.*")))
    return imgFile

def resize():
    from PIL import Image
    try:
        file=Image.open(fileName())
        file.save('Original.jpeg')
        #img=file.resize((100,100),Image.ANTIALIAS)  # Resizing Image
        file.thumbnail((100,100), Image.ANTIALIAS)  # Thumbnail Generator
        file.save('Resized.jpeg')
        print 'Done.'
    except:
        print 'No Image Selected...!'



if __name__ == '__main__':
    resize()
