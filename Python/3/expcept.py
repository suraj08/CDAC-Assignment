class demoException(Exception):
    def __init__(self,msg):
        self.msg=msg

level = -1

try:
    if level < 1:
        raise demoException("Invalid level : {}".format(level))

except demoException,e:
    print(e.msg)
