

def impStack():
    from stack import stack
    s=stack()
    print '\nWelcome to STACK\n'
    while(1):
        sel=int(raw_input('1.Push\t2.Pop\t3.Top\t4.Exit\n'))

        if sel==1:
            s.push(raw_input('Enter Something in Stack\n'))
        elif sel==2:
            s.pop()
        elif sel==3:
            s.top()
        elif sel==4:
            print 'Back to Datastructure'
            break

def impQueue():
    from queue import queue
    q=queue()
    print '\nWelcome to QUEUE\n'
    while(1):
        sel=int(raw_input('1.Enqueue\t2.Dequeue\t3.Front\t4.Exit\n'))

        if sel==1:
            q.enqueue(raw_input('Enter Something in Queue\n'))
        elif sel==2:
            q.dequeue()
        elif sel==3:
            q.front()
        elif sel==4:
            print 'Back to Datastructure'
            break

def main():
    print 'Welcome to DataStructure'
    while(True):
        req=int(raw_input('\n1.Stack\t2.Queue\t3.Exit\n'))

        if req==int(1):
            impStack()

        elif req==2:
            impQueue()

        elif req==3:
            print 'Bye...!'
            exit(0)

if __name__=='__main__':
    main()
