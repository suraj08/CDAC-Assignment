import time,logging

logging.basicConfig(filename='stackLog.log',level=logging.DEBUG)

class stack:

    def __init__(self):
        self.items=[]

    def isEmpty(self):
        return self.items == []

    def push(self,item):
        self.items.append(item)
        logging.debug(time.asctime(time.localtime(time.time()))+ "--- pushed "+str(item))

    def pop(self):
        if not self.isEmpty():
            temp = self.items.pop()
            print temp
            logging.debug(time.asctime(time.localtime(time.time()))+ "--- poped "+str(temp))
        else:
            print('Stack is Empty...!\n'
                  'Please Insert Something is Stack first.')

    def top(self):
        if not self.isEmpty():
            print self.items[len(self.items)-1]
        else:
            print('Stack is Empty...!\n')
