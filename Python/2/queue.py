import time,logging

logging.basicConfig(filename='queueLog.log',level=logging.DEBUG)

class queue:

    def __init__(self):
        self.items = []

    def isEmpty(self):
        return self.items == []

    def enqueue(self, item):
        self.items.append(item)
        logging.debug(time.asctime(time.localtime(time.time()))+ "--- enqueue "+str(item))

    def dequeue(self):
        if not self.isEmpty():
            temp= self.items.pop(0)
            logging.debug(time.asctime(time.localtime(time.time()))+ "--- dequeue "+str(temp))
            return temp
        else:
            print('Queue is Empty...!\n'
                  'Please Insert Something is Queue first.')

    def front(self):
        if not self.isEmpty():
            print self.items[0]
        else:
            print('Queue is Empty...!\n')
